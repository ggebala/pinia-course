import { defineStore } from 'pinia';

export const useAuthUserStore = defineStore('AuthUserStore', {
    state: () => {
        return {
            username: "johny_io"
        }    
    },
    getters: {
    },
    actions: {
        visitTwitterPrifile() {
            window.open(`https://www.twitter.com/${this.username}`, '_blank')
        }
    },
});
